//
//  OSMDisplay.swift
//  krasnale
//
//  Created by Jakub Tomanik on 27/11/2014.
//  Copyright (c) 2014 Jakub Tomanik. All rights reserved.
//
import CoreGraphics

// MARK: OSM layer objects
class OSMLayer {
  let document: OSMDocument?
  var shapes: Dictionary<Int, OSMShape>
  var stylesPath: String
  var styles: Array<OSMRendererStyle>
  var backgroundStyle:OSMRendererStyle?
  var backgroundShape:OSMBoundsShape?
  var map:MKMapView?
  
  init() {
    self.document = OSMDocument()
    self.shapes = [Int: OSMShape]()
    self.stylesPath = ""
    self.styles = [OSMRendererStyle]()
  }
  init?(path: String){
    self.document = OSMDocument(path: path)
    self.shapes = [Int: OSMShape]()
    self.styles = [OSMRendererStyle]()
    self.stylesPath = ""
    if self.document == nil { return nil }
  }
  
  func loadStyles(path: String) -> Void {
    // check if file exists at the path, return if not
    let bundle = NSBundle.mainBundle()
    let manager = NSFileManager.defaultManager()
    if (!manager.fileExistsAtPath(path)) { return }
    let stylesXML = RXMLElement(fromXMLFilePath: path)
    
    // check if first XML element is an "style" tag, return if not
    if stylesXML.tag != "style" { return }
    self.stylesPath = path
    
    let backgroundStyle = stylesXML.child("background").child("style")
    if let background = OSMRendererStyle(element: backgroundStyle) {
      self.backgroundStyle = background
      //self.styles.append(background)
    }
    
    let styleLayers = stylesXML.child("layers").children("layer")
    for layer in styleLayers {
      let styles = layer.children("style")
      for style in styles {
        if let newStyle = OSMRendererStyle(element: style as RXMLElement) {
          self.styles.append(newStyle)
        }
      }
    }
    
  }
  
  func buildShapes() {
    if self.document!.bounds != nil {
      if let backgroundShape = OSMBoundsShape(bounds: self.document!.bounds!) {
        if backgroundShape.build(self) {
          self.backgroundShape = backgroundShape
        }
      }
    }
    
    self.document!.elements.each{ key, value in
      if let element = value as? OSMWay {
        if let shape = OSMWayShape(way: element) {
          if shape.build(self) {
            self.shapes[key] = shape
          }
        }
      }
    }
    
    self.document!.elements.each{ key, value in
      if let element = value as? OSMRelation {
        if let shape = OSMRelationShape(relation: element) {
          if shape.build(self) {
            self.shapes[key] = shape
          }
        }
      }
    }
    
  }
  
  func renderShapesInMap(view: MKMapView) -> Int{
    self.map = view
    
    if self.backgroundShape != nil && self.backgroundStyle != nil {
      self.backgroundShape!.style = self.backgroundStyle!
      self.backgroundShape!.stylise()
      if let shape = self.backgroundShape?.shape as? MKPolygon {
        view.addOverlay(shape)
      }
    }
    
    for style in self.styles {
      println(style.styleHash)
      self.shapes.each { key, value in
        if key == 0 { return }
        if value.element!.matchKeyValue(style.styleHash) {
          value.style = style
          value.stylise()
          switch value.shape {
          case let shape as MKPolygon:
            view.addOverlay(shape)
            if value.element!.matchKeyValue("app:touchable=yes") {
              view.addAnnotation(shape)
            }
          case let shape as MKPolyline:
            view.addOverlay(shape)
          default:
            break
          }
        }
      }
    }
    
    return 0
  }
  
  func getRendererForOverlay(overlay: MKOverlay) -> MKOverlayRenderer? {
    if let id = overlay.title!.toInt() {
      if id == 0 {
        return self.backgroundShape?.renderer
      } else {
        let shape = self.shapes[id]
        return shape?.renderer!
      }
    }
    return nil
  }
  
  func checkForTouch(point: MKMapPoint) -> OSMShape? {
    let id:Int = 0
    let touched = self.shapes.filter { (key, value) -> Bool in
      let touchable = value.element!.matchKeyValue("app:touchable=yes")
      if touchable {
        let inside = value.intersectMapPoint(point)
        if  inside {
          let cgpoint = self.map!.convertCoordinate(MKCoordinateForMapPoint(point), toPointToView: nil)
          let result =  value.containsPointOnMap(point)
          return result
        }
      }
      return false
    }
    
    if touched.count > 0 {
      let ret = touched.values.first
      return ret
    }
    return nil
  }
}
class OSMShape {
  var style:OSMRendererStyle?
  var shape:MKShape?
  var renderer:MKOverlayRenderer?
  var element:OSMElement?
  
  init() {
    
  }
  
  func stylise() -> Bool {
    return true
  }
  func render() -> Bool {
    return true
  }
  
  func intersectMapRect(rect: MKMapRect) -> Bool {
    if let overlay = self.shape as? MKOverlay {
      return MKMapRectContainsRect(overlay.boundingMapRect, rect)
    }
    return false
  }
  
  func intersectMapPoint(point: MKMapPoint) -> Bool {
    if let overlay = self.shape as? MKOverlay {
      return MKMapRectContainsPoint(overlay.boundingMapRect, point)
    }
    return false
  }
  func containsPointOnMap(point: MKMapPoint) -> Bool {
    if let overlayRenderer = self.renderer as? MKOverlayPathRenderer {
      let hit = overlayRenderer.pointForMapPoint(point)
      let path = overlayRenderer.path
      let test = CGPathGetCurrentPoint(path)
      return CGPathContainsPoint(path, nil, hit, false)
    }
    return false
  }
  
  func fillinCallout(callout: SMCalloutView) {
    callout.title = self.element?.tags["app:title"]
    //callout.subtitle = self.element?.tags["app:class"]
    //callout.rightAccessoryView.userInteractionEnabled = false
    callout.leftAccessoryView.backgroundColor = self.style!.fill.fillColor
    if self.element!.tags.has("app:description") {
      callout.subtitle = self.element?.tags["app:description"]
    } else {
      callout.subtitle = ""
    }
    if self.element!.tags.has("app:icon") {
      
      let imageName = self.element!.tags["app:icon"]!
      let filename = "\(imageName).png"
      if let image = UIImage(named: filename) {
        if let icon = callout.leftAccessoryView.viewWithTag(44) as? UIImageView {
          icon.image = image
        }
      }
      
    } else {
      callout.leftAccessoryView.userInteractionEnabled = true
    }
  }
  
}

class OSMWayShape: OSMShape {
  var way: OSMWay?
  
  var nodesCoordinates:Array<CLLocationCoordinate2D>
  
  override init () {
    self.way = OSMWay()
    self.nodesCoordinates = [CLLocationCoordinate2D]()
    super.init()
    self.element = self.way
  }
  
  init? (way: OSMWay) {
    self.nodesCoordinates = [CLLocationCoordinate2D]()
    super.init()
    if way.id == 0  || !way.built { return nil }
    
    self.way = way
    self.element = self.way
  }
  
  override func stylise() -> Bool {
    if self.way!.visible == false {
      return false
    }
    
    switch self.shape {
    case let shape as MKPolygon:
      self.renderer = MKPolygonRenderer(polygon: shape)
      self.style!.styliseRenderer(self.renderer!)
      return true
    case let shape as MKPolyline:
      self.renderer = MKPolylineRenderer(polyline: shape)
      self.style!.styliseRenderer(self.renderer!)
      return true
    default:
      return false
    }
  }
  
  override func render() -> Bool {
    return true
  }
  
  func build(layer: OSMLayer) -> Bool {
    self.nodesCoordinates = self.way!.nodes.map({ (node:OSMNode) -> CLLocationCoordinate2D in
      return node.node
    })
    
    if self.way!.closed {
      let polygon = MKPolygon(coordinates: &self.nodesCoordinates, count: self.nodesCoordinates.count)
      polygon.title = String(self.way!.id)
      self.shape = polygon
      return true
    } else {
      self.nodesCoordinates = self.way!.nodes.map({ (node:OSMNode) -> CLLocationCoordinate2D in
        return node.node
      })
      let polygon = MKPolyline(coordinates: &self.nodesCoordinates, count: self.nodesCoordinates.count)
      polygon.title = String(self.way!.id)
      self.shape = polygon
      return true
    }
  }
}

class OSMBoundsShape: OSMShape {
  var bounds: OSMBounds?
  var margin = 1.0
  
  var nodesCoordinates:Array<CLLocationCoordinate2D>
  
  override init () {
    self.bounds = OSMBounds()
    self.nodesCoordinates = [CLLocationCoordinate2D]()
    super.init()
  }
  
  init? (bounds: OSMBounds) {
    if bounds.maxLatitude != bounds.minLatitude &&
      bounds.maxLongitude != bounds.minLongitude
    {
      self.bounds = bounds
      self.nodesCoordinates = [CLLocationCoordinate2DMake(bounds.maxLatitude+self.margin, bounds.minLongitude-self.margin),
        CLLocationCoordinate2DMake(bounds.maxLatitude+self.margin, bounds.maxLongitude+self.margin),
        CLLocationCoordinate2DMake(bounds.minLatitude-self.margin, bounds.maxLongitude+self.margin),
        CLLocationCoordinate2DMake(bounds.minLatitude-self.margin, bounds.minLongitude-self.margin)
      ]
      super.init()
    } else {
      self.bounds = OSMBounds()
      self.nodesCoordinates = [CLLocationCoordinate2D]()
      super.init()
      return nil
    }
  }
  
  override func stylise() -> Bool {
    
    if let shape = self.shape as? MKPolygon {
      self.renderer = MKPolygonRenderer(polygon: shape)
      self.style!.styliseRenderer(self.renderer!)
      return true
    } else {
      return false
    }
  }
  
  override func render() -> Bool {
    return true
  }
  
  func build(layer: OSMLayer) -> Bool {
    
    let polygon = MKBackgroundOverlay(coordinates: &self.nodesCoordinates, count: self.nodesCoordinates.count)
    polygon.title = String(0)
    self.shape = polygon
    return true
    
  }
}

class OSMRelationShape: OSMShape {
  var relation: OSMRelation?
  
  override init () {
    super.init()
    self.element = self.relation
  }
  
  init? (relation: OSMRelation) {
    super.init()
    if relation.id == 0  || !relation.built { return nil }
    
    self.relation = relation
    self.element = self.relation
  }
  
  override func stylise() -> Bool {
    if self.relation!.visible == false {
      return false
    }
    
    switch self.shape {
    case let shape as MKPolygon:
      self.renderer = MKPolygonRenderer(polygon: shape)
      self.style!.styliseRenderer(self.renderer!)
      return true
    case let shape as MKPolyline:
      self.renderer = MKPolylineRenderer(polyline: shape)
      self.style!.styliseRenderer(self.renderer!)
      return true
    default:
      return false
    }
  }
  
  override func render() -> Bool {
    return true
  }
  
  func build(layer: OSMLayer) -> Bool {
    
    if self.relation!.outer.count == 1 && self.relation!.inner.count == 0 {
      // no cutouts
      var outerId = self.relation!.outer[0].id
      var outerWay = layer.shapes[outerId] as OSMWayShape
      let polygon = outerWay.shape
      polygon!.title = String(self.relation!.id)
      self.shape = polygon
      return true
      
    } else if self.relation!.outer.count == 1 && self.relation!.inner.count > 0 {
      // cutouts
      
      let cutOuts = self.relation!.inner.map({ (element: OSMWay) -> MKPolygon in
        let way = element as OSMWay
        let wayShape = layer.shapes[way.id]
        let ret = wayShape!.shape as MKPolygon
        return ret
      })
      var outerId = self.relation!.outer[0].id
      var outerWay = layer.shapes[outerId] as OSMWayShape
      var outerNodes:[CLLocationCoordinate2D] = outerWay.nodesCoordinates
      let polygon = MKPolygon(coordinates: &outerNodes, count: outerNodes.count, interiorPolygons: cutOuts)
      polygon.title = String(self.relation!.id)
      self.shape = polygon
      return true
    } else if self.relation!.outer.count > 1 && self.relation!.inner.count == 0 {
      for way in self.relation!.outer {
        let outer = way as OSMWay
        outer.tags = self.relation!.tags
        outer.bufferTags()
        var outerId = outer.id
        if let outerWay = OSMWayShape(way: outer) {
          outerWay.build(layer)
        }
      }
      return true
    } else {
      // error
      return false
    }
  }
}

// MARK: OSM style objects
struct OSMLineStyle {
  var lineWidth:CGFloat
  var strokeColor: UIColor!
  
  init() {
    self.lineWidth = 0.0
    self.strokeColor = UIColor.clearColor()
  }
  mutating func parse (element: RXMLElement) -> Void {
    if element.tag != "line" { return }
    
    self.lineWidth = CGFloat(element.attributeAsDouble("width"))
    let colorR = CGFloat(element.attributeAsInt("r"))/255
    let colorG = CGFloat(element.attributeAsInt("g"))/255
    let colorB = CGFloat(element.attributeAsInt("b"))/255
    self.strokeColor = UIColor(red: colorR, green: colorG, blue: colorB, alpha: 1.0)
  }
}

struct OSMFillStyle {
  var fillColor: UIColor!
  
  init() {
    self.fillColor = UIColor.clearColor()
  }
  mutating func parse (element: RXMLElement) -> Void {
    if element.tag != "fill" { return }
    
    let colorR = CGFloat(element.attributeAsInt("r"))/255
    let colorG = CGFloat(element.attributeAsInt("g"))/255
    let colorB = CGFloat(element.attributeAsInt("b"))/255
    self.fillColor = UIColor(red: colorR, green: colorG, blue: colorB, alpha: 1.0)
  }
}

class OSMRendererStyle {
  var key:String
  var value:String
  var styleHash:String
  
  var line: OSMLineStyle
  var fill: OSMFillStyle
  
  init () {
    self.key = ""
    self.value = ""
    self.styleHash = ""
    self.line = OSMLineStyle()
    self.fill = OSMFillStyle()
  }
  init?(element: RXMLElement){
    self.key = ""
    self.value = ""
    self.styleHash = ""
    self.line = OSMLineStyle()
    self.fill = OSMFillStyle()
    self.parse(element)
    
    if (self.key == "") { return nil }
  }
  func parse (element: RXMLElement) -> Bool {
    if element.tag != "style" { return false}
    
    self.key = element.attribute("k")
    self.value = element.attribute("v")
    self.styleHash = self.key+"="+self.value
    
    self.line.parse(element.child("line"))
    self.fill.parse(element.child("fill"))
    
    return true
  }
  
  func keyValue() -> String {
    return self.key+"="+self.value
  }
  
  func styliseRenderer(renderer: MKOverlayRenderer) -> Void {
    switch renderer {
    case let custom as MKPolygonRenderer:
      custom.strokeColor = self.line.strokeColor
      custom.fillColor = self.fill.fillColor
      custom.lineWidth = self.line.lineWidth
    case let custom as MKPolylineRenderer:
      custom.strokeColor = self.line.strokeColor
      custom.lineWidth = self.line.lineWidth
    case let custom as MKCircleRenderer:
      custom.strokeColor = self.line.strokeColor
      custom.fillColor = self.fill.fillColor
      custom.lineWidth = self.line.lineWidth
    default:
      return
    }
  }
}

/*
// add OSM data to the MapView
var defaultStyle = RendererStyle()
if let found = xmlStylesArray.takeFirst({$0.key == "default" && $0.value == "style"}) {
defaultStyle = found
}

// render relations / multishapes
for (id, relation) in osmRelationsDictionary {
var style:RendererStyle?

// find first style for this relation
for (key, value) in relation.tags {
if let found = xmlStylesArray.takeFirst({$0.key == key && $0.value == value}) {
style = found
break
}
}

// if no style found use default one
if style == nil { style = defaultStyle }

if relation.inner.count > 0 {
// create Polygon with cut outs
var interior = [MKPolygon]()
for poly in relation.inner {
let newPoly = MKPolygon(coordinates: &poly.nodes, count: poly.nodes.count)
interior.append(newPoly)
}
if relation.outer.count == 1 {
let outerWay:OSMWay = relation.outer.first!
let outPoly = MKPolygon(coordinates: &outerWay.nodes, count: outerWay.nodes.count, interiorPolygons: interior)
outPoly.title = String(relation.id)

let polygonRenderer = MKPolygonRenderer(polygon: outPoly)
style?.styliseRenderer(polygonRenderer)

self.overlays[outPoly] = polygonRenderer
self.mapView.addOverlay(outPoly)
}

} else if relation.outer.count > 1 {
for poly in relation.outer {
let outPoly = MKPolygon(coordinates: &poly.nodes, count: poly.nodes.count)
outPoly.title = String(relation.id)

let polygonRenderer = MKPolygonRenderer(polygon: outPoly)
style?.styliseRenderer(polygonRenderer)

self.overlays[outPoly] = polygonRenderer
self.mapView.addOverlay(outPoly)
}
}
}

// render ways / shapes
for (id, way) in osmWaysDictionary {
var style:RendererStyle?

// find first style for this way
for (key, value) in way.tags {
if let found = xmlStylesArray.takeFirst({$0.key == key && $0.value == value}) {
style = found
break
}
}

// if no style found use default one
if style == nil { style = defaultStyle }

if way.closed {
let outPoly = MKPolygon(coordinates: &way.nodes, count: way.nodes.count)
outPoly.title = String(way.id)

let polygonRenderer = MKPolygonRenderer(polygon: outPoly)
style?.styliseRenderer(polygonRenderer)

self.overlays[outPoly] = polygonRenderer
self.mapView.addOverlay(outPoly)
} else {
let polyline = MKPolyline(coordinates: &way.nodes, count: way.nodes.count)
polyline.title = String(way.id)
let polylineRenderer = MKPolylineRenderer(polyline: polyline)
style?.styliseRenderer(polylineRenderer)

self.overlays[polyline] = polylineRenderer
self.mapView.addOverlay(polyline)
}
*/


