//
//  OSMModel.swift
//  krasnale
//
//  Created by Jakub Tomanik on 25/11/2014.
//  Copyright (c) 2014 Jakub Tomanik. All rights reserved.
//



// OSM file parser

// MARK: OSM elements
// OSM structs encapsulate elements
struct OSMBounds {
  var minLatitude:CLLocationDegrees = 0.0
  var minLongitude:CLLocationDegrees = 0.0
  var maxLatitude:CLLocationDegrees = 0.0
  var maxLongitude:CLLocationDegrees = 0.0
  var origin:String?
  
  init() {
    
  }
  
  init?(element: RXMLElement) {
    self.parse(element)
    if self.maxLatitude == self.minLatitude &&
      self.maxLongitude == self.minLongitude
    {
      return nil
    }
  }
  
  mutating func parse (element: RXMLElement) -> Void {
    if element.tag != "bounds" { return }
    
    self.minLatitude = element.attributeAsDouble("minlat")
    self.maxLatitude = element.attributeAsDouble("maxlat")
    self.minLongitude = element.attributeAsDouble("minlon")
    self.maxLongitude = element.attributeAsDouble("maxlon")
    self.origin = element.attribute("origin")
  }
}

struct OSMMember {
  var type:String?
  var ref:Int
  var role:String?
  
  init() {
    self.ref = 0
  }
  
  init?(element: RXMLElement) {
    self.ref = 0
    self.parse(element)
    
    if self.ref == 0 || self.type == "" {return nil}
  }
  
  mutating func parse (element: RXMLElement) -> Void {
    if element.tag != "member" { return }
    
    self.type = element.attribute("type")
    self.ref = element.attributeAsInt("ref")
    self.role = element.attribute("role")
  }
}

// MARK: OSM document objects
// OSM document root model
class OSMDocument {
  var path:String
  var bounds:OSMBounds?
  var elements:Dictionary<Int, OSMElement>
 
  
  // designated initialiser, creates empty model
  init() {
    self.path = ""
    self.elements = [Int: OSMElement]()
  }
  
  // failable initialiser, intended for primary use
  // if file at path doesn't exist or is misformatted / corrupted it should fail to initialise class
  // in that case it returns nil instead of instance
  init?(path: String){
    
    // it is language requirement to initialise class' stored properties first
    // and we cant call designated initialiser here :(
    self.path = path
    self.elements = [Int: OSMElement]()
    
    
    // check if file exists at the path, fail if not
    let bundle = NSBundle.mainBundle()
    let manager = NSFileManager.defaultManager()
    if (!manager.fileExistsAtPath(path)) { return nil }
    let osmXML = RXMLElement(fromXMLFilePath: path)
    
    // check if first XML element is an "osm" tag, fail if not
    if osmXML.tag != "osm" { return nil }
    
    let boundsTag = osmXML.child("bounds")
    if let osmBound = OSMBounds(element: boundsTag) {
      self.bounds = osmBound
    }
    
    
    // parse nodes from OSM data file
    // file can have no nodes
    let osmNodes = osmXML.children("node")
    for osmNode in osmNodes {
      if let node = OSMNode(element: osmNode as RXMLElement) {
        self.elements[node.id] = node
      }
    }
    
    // parse ways from OSM data file
    // we parse ways without chcecking if we have any nodes
    // beacuse that doesn't voilate OSM file format spec
    // will have to throw error later if we have ways with non existing nodes
    let osmWays = osmXML.children("way")
    for osmWay in osmWays {
      if let way = OSMWay(element: osmWay as RXMLElement) {
        self.elements[way.id] = way
      }
    }
    
    // parse relations from OSM data file
    // there can be blank relations with no ways / nodes in them
    let osmRelations = osmXML.children("relation")
    for osmRelation in osmRelations {
      if let relation = OSMRelation(element: osmRelation as RXMLElement) {
        self.elements[relation.id] = relation
      }
    }
    
    self.evaluateElements()
  }
  
  func evaluateElements() {
    self.elements.each{ key, value in
      switch value {
      case let element as OSMWay:
        element.evaluate(self)
      case let element as OSMRelation:
        element.evaluate(self)
      default:
        break
      }
    }
  }
  
}

// base class for all OSM objects
class OSMElement {
  var id:Int
  var tags:Dictionary<String, String> // this is wrong, what if k=v1 and k=v2 are valid?
  var visible:Bool
  
  private var tagBuffer:Array<String>
  
  init (id: Int, tags:Dictionary<String, String>) {
    self.id = id
    self.tags = tags
    self.tagBuffer = [String]()
    self.visible = false
    self.bufferTags()
  }
  convenience init () {
    self.init(id: 0, tags: ["":""])
  }
  init? (element: RXMLElement) {
    self.id = 0
    self.tags = [String: String]()
    self.tagBuffer = [String]()
    self.visible = false
    
    if !self.parseElement(element) {return nil}
  }
  func parseElement (element: RXMLElement) -> Bool {
    //println(element.xml)
    let dbg = element.attributeAsInt("id")
    self.id = dbg
    if self.id == 0 { return false}
    
    let osmTags = element.children("tag")
    for osmTag in osmTags {
      let key = osmTag.attribute("k")
      let value = osmTag.attribute("v")
      self.tags[key] = value
      self.tagBuffer.append(key+"="+value)
    }
    self.visible = element.attribute("visible") == "true"
    
    return true
  }
  func matchKeyValue(hash: String) -> Bool {
    return self.tagBuffer.any { $0 == hash }
  }
  
  func bufferTags() {
    self.tagBuffer.removeAll(keepCapacity: false)
    for (key, value) in self.tags {
      self.tagBuffer.append(key+"="+value)
    }
  }
  
}

// OSM node
class OSMNode: OSMElement {
  var node:CLLocationCoordinate2D
  
  init (id: Int, tags:Dictionary<String, String>, node: CLLocationCoordinate2D) {
    self.node = node
    super.init(id: id, tags: tags)
  }
  convenience init (node: CLLocationCoordinate2D) {
    self.init(id: 0, tags: ["":""], node: node)
  }
  convenience init () {
    let zeroNode = CLLocationCoordinate2D(
      latitude: 0,
      longitude: 0
    )
    self.init(id: 0, tags: ["":""], node: zeroNode)
  }
  override init? (element: RXMLElement) {
    let zeroNode = CLLocationCoordinate2D(
      latitude: 0,
      longitude: 0
    )
    self.node = zeroNode
    
    super.init(element: element)
    if !self.parseNode(element) {return nil}
  }
  func parseNode(element: RXMLElement) -> Bool {
    let nodeLocation = CLLocationCoordinate2D(
      latitude: element.attributeAsDouble("lat"),
      longitude: element.attributeAsDouble("lon")
    )
    self.node = nodeLocation
    return true
  }
  
}

class OSMWay: OSMElement {
  var nodes:Array<OSMNode>
  var closed:Bool
  var inRelation:Bool
  var built:Bool
  
  private var refs:Array<Int>
  
  override init (id: Int, tags:Dictionary<String, String>) {
    self.nodes = [OSMNode]()
    self.refs = [Int]()
    self.closed = false
    self.inRelation = false
    self.built = false
    super.init(id: id, tags: tags)
  }
  
  convenience init (id: Int, tags:Dictionary<String, String>, nodes:Array<CLLocationCoordinate2D>, closed:Bool) {
    self.init(id: id, tags: tags)
    nodes.each{ key, value in
      let node = OSMNode(node: value)
      self.nodes.append(node)
    }
    self.closed = closed
    self.built = true
  }
  override init? (element: RXMLElement) {
    self.nodes = [OSMNode]()
    self.refs = [Int]()
    self.closed = false
    self.inRelation = false
    self.built = false
    
    super.init(element: element)
    if !self.parseWay(element) {return nil}
  }
  
  func parseWay(element: RXMLElement) -> Bool {
    let nd = element.children("nd")
    if nd.count == 0 {return false}
    
    for ref in nd {
      let newNode = ref.attributeAsInt("ref")
      if newNode != 0 {
        self.refs.append(newNode)
      }
    }
    if self.refs.count == 0 {return false}
    
    if (self.refs.first? == self.refs.last?) {
      self.closed = true
    }
    
    return true
  }
  
  func evaluate(document: OSMDocument) -> Bool {
    for ref in self.refs {
      if let element = document.elements[ref] as? OSMNode {
        self.nodes.append(element)
      } else {
        self.nodes.removeAll(keepCapacity: false)
        return false
      }
    }
    if self.closed {
      self.nodes.removeLast()
    }
    self.built = true
    return true
  }
}
class OSMRelation: OSMElement {
  var outer:Array<OSMWay>
  var inner:Array<OSMWay>
  var built:Bool
  
  private var members:Array<OSMMember>
  
  override init (id: Int, tags:Dictionary<String, String>) {
    self.inner = [OSMWay]()
    self.outer = [OSMWay]()
    self.members = [OSMMember]()
    self.built = false
    super.init(id: id, tags: tags)
  }
  override init? (element: RXMLElement) {
    self.inner = [OSMWay]()
    self.outer = [OSMWay]()
    self.members = [OSMMember]()
    self.built = false
    
    super.init(element: element)
    if !self.parseRelation(element) {return nil}
  }
  func parseRelation(element: RXMLElement) -> Bool {
    let members = element.children("member")
    for member in members {
      if let newMember = OSMMember(element: member as RXMLElement) {
        self.members.append(newMember)
      }
    }
    return true
  }
  func evaluate(document: OSMDocument) -> Bool {
    
    for member in self.members {
      switch member.type! {
      case "way":
        if let way = document.elements[member.ref] as? OSMWay {
          way.inRelation = true
          switch member.role! {
          case "inner":
            self.inner.append(way)
          default:
            self.outer.append(way)
          }
        }
      default:
        return false
      }
    }
    self.built = true
    return true
  }
}