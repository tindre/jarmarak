//
//  ViewController.swift
//  krasnale
//
//  Created by Jakub Tomanik on 14/11/2014.
//  Copyright (c) 2014 Jakub Tomanik. All rights reserved.
//
import Foundation
import UIKit
import CoreLocation
import MapKit

class ViewController: UIViewController, MKMapViewDelegate, SMCalloutViewDelegate, CLLocationManagerDelegate {
  
  @IBOutlet weak var mapView: SMMapView!
  
  
  var left:Double = 0
  var right:Double = 0
  var top:Double = 0
  var bottom:Double = 0

  var changingMap = true
  
  var mapBoundsRect:MKMapRect?
  var mapBoundsRegion:MKCoordinateRegion?
  var mapFocusPoint:CLLocationCoordinate2D  = CLLocationCoordinate2D(latitude: 0, longitude: 0)
  
  var currentViewRect:MKMapRect?
  var currentViewRegion:MKCoordinateRegion?
  
  var defaultViewRect:MKMapRect?
  var defaultViewRegion:MKCoordinateRegion?
  
  var currentMapCenter:CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 0, longitude: 0)
  var currentMapSpan:MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: 0.001, longitudeDelta: 0.001)
  
  var mapViewBounds:MKMapRect?
  
  var callout:SMCalloutView?
  var annotationView: MKAnnotationView?
  
  let locationManager = CLLocationManager()
  
  //var overlays = [MKShape: MKOverlayPathRenderer]()
  
  var layers = [OSMLayer]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    self.locationManager.delegate = self
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
    locationManager.requestWhenInUseAuthorization()
    
    let delegate = UIApplication.sharedApplication().delegate as AppDelegate
    
    // Gesture recognisers
    let oneTap = UITapGestureRecognizer(target: self, action:Selector("handleTap:"))
    oneTap.cancelsTouchesInView = false
    oneTap.numberOfTapsRequired = 1
    
    let doubleTap = UITapGestureRecognizer(target: self, action:Selector("handleTap:"))
    doubleTap.cancelsTouchesInView = false
    doubleTap.numberOfTapsRequired = 2
    
    let moreTap = UITapGestureRecognizer(target: self, action:Selector("moreTap"))
    moreTap.numberOfTapsRequired = 1
    
    let iconTap = UITapGestureRecognizer(target: self, action:Selector("iconTap"))
    iconTap.numberOfTapsRequired = 1
    
    self.mapView.addGestureRecognizer(oneTap)
    self.mapView.addGestureRecognizer(oneTap)
    
    oneTap.requireGestureRecognizerToFail(doubleTap)
    
    // config mapView
    self.mapView.delegate = self
    
    delegate.configXML!.iterateWithRootXPath("/settings/app/mapView/mapType", usingBlock: {(found: RXMLElement!) in
      self.mapView.mapType = MKMapType(rawValue: UInt(found.textAsInt))!
    })
    
    let xmlCenter = delegate.configXML!.child("map").child("center")
    let xmlLat = xmlCenter.attributeAsDouble("lat")
    let xmlLon = xmlCenter.attributeAsDouble("lon")
    if xmlLat != 0 && xmlLon != 0 {
      self.mapFocusPoint.latitude = xmlLat
      self.mapFocusPoint.longitude = xmlLon
    }
    
    let xmlBounds = delegate.configXML!.child("map").child("bounds")
    if let tmpBounds = OSMBounds(element: xmlBounds) {
      self.top = tmpBounds.maxLatitude
      self.bottom = tmpBounds.minLatitude
      self.left = tmpBounds.minLongitude
      self.right = tmpBounds.maxLongitude
      
      let originCoordinate = CLLocationCoordinate2D(latitude: self.top, longitude: self.left)
      let maxCoordinate = CLLocationCoordinate2D(latitude: self.bottom, longitude: self.right)
      let originMapPoint = MKMapPointForCoordinate(originCoordinate)
      let maxMapPoint = MKMapPointForCoordinate(maxCoordinate)
      
      self.mapBoundsRect = MKMapRectMake(originMapPoint.x, originMapPoint.y, maxMapPoint.x - originMapPoint.x, maxMapPoint.y - originMapPoint.y)
      self.mapBoundsRegion = MKCoordinateRegionForMapRect(self.mapBoundsRect!)
      
      
      let initialRegion = MKCoordinateRegionMake(self.mapFocusPoint, self.currentMapSpan)
      self.mapView.setRegion(initialRegion, animated: false)
      saveMapState()
      self.defaultViewRect = self.mapView.visibleMapRect
      self.defaultViewRegion = self.mapView.region
    }
    
    // Load layers
    let xmlLayers = delegate.configXML!.child("map").child("layers").children("layer")
    for xmlLayer in xmlLayers {
      let layerSource = xmlLayer.attribute("src")
      let layerStyleSource = xmlLayer.attribute("style")
      let layerPath = delegate.payloadPath! + layerSource
      let layerStylePath = delegate.payloadPath! + layerStyleSource
      
      if let layer = OSMLayer(path: layerPath) {
        self.layers.append(layer)
        layer.loadStyles(layerStylePath)
        layer.buildShapes()
        layer.renderShapesInMap(self.mapView)
      }
    }
    
    self.callout = SMCalloutView.platformCalloutView()
    self.callout!.delegate = self
    self.callout!.title = "title"
    self.callout!.subtitle = "subtitle"
    let leftView = UIButton(frame: CGRectMake(0, 0, 44, 44+30))
    leftView.backgroundColor = UIColor(red: 0, green: 0.5, blue: 1, alpha: 1)
    if let icon = UIImage(named: "choinka1.png") {
      let iconView = UIImageView(image: icon)
      iconView.frame = CGRectMake(0, 0, 44, 44)
      iconView.tag = 44
      leftView.addSubview(iconView)
    }
    
    leftView.addGestureRecognizer(iconTap)
    self.callout?.leftAccessoryView = leftView
    
    self.mapView!.calloutView = self.callout!
    self.changingMap = false
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  // MARK: private methodes
  
  func handleTap(recognizer: UITapGestureRecognizer) {
    if recognizer.state == .Ended {
      
      // CG Units
      let tapPoint = recognizer.locationInView(self.mapView)
      let tapCoordinate = self.mapView.convertPoint(tapPoint, toCoordinateFromView: self.mapView)
      let tapMapPoint = MKMapPointForCoordinate(tapCoordinate)

      for layer in self.layers {
        if let touched = layer.checkForTouch(tapMapPoint) {
          println("touched: \(touched.element!.id)")
          touched.fillinCallout(self.callout!)
          self.mapView!.selectAnnotation(touched.shape as MKAnnotation, animated: true)
        } else {
          //self.callout!.dismissCalloutAnimated(true)
        }
      }
    }
  }
  
  func iconTap() {
    println("iconTap")
  }
  
  func moreTap() {
    println("moreTap")
  }
  
  func saveMapState(){
    self.currentViewRegion = self.mapView.region
    self.currentViewRect = self.mapView.visibleMapRect
  }
  
  
  func checkScroll() {
    if self.mapView!.centerCoordinate.latitude < 50 || self.mapView!.centerCoordinate.latitude > 52 { return }
    
    let map = self.mapView!
    let visibleMapRect = map.visibleMapRect
    let visibleRegion = map.region
    
    // nothing to do
    if MKMapRectContainsRect(self.mapBoundsRect!, visibleMapRect) { return }
    
    //check if zoomed out too much
    if visibleMapRect.size.width > self.mapBoundsRect!.size.width ||
      visibleMapRect.size.height > self.mapBoundsRect!.size.height {
        map.setVisibleMapRect(self.defaultViewRect!, animated: true)
    } else {
      // we are fully within and intersect
      let visibleWestDelta  = MKMapRectGetMinX(visibleMapRect) - MKMapRectGetMinX(self.mapBoundsRect!)  // ngative out of bounds
      let visibleEastDelta = MKMapRectGetMaxX(self.mapBoundsRect!) - MKMapRectGetMaxX(visibleMapRect)  // ngative out of bounds
      let visibletNorthDelta = MKMapRectGetMaxY(self.mapBoundsRect!) - MKMapRectGetMaxY(visibleMapRect) // ngative out of bounds
      let visibleSouthDelta = MKMapRectGetMinY(visibleMapRect) - MKMapRectGetMinY(self.mapBoundsRect!) // ngative out of bounds
      
      var deltaX = 0.0
      var deltaY = 0.0
      
      if visibleWestDelta < 0 {
        deltaX -= visibleWestDelta
      }
      if visibleEastDelta < 0 {
        deltaX += visibleEastDelta
      }
      if visibletNorthDelta < 0 {
        deltaY += visibletNorthDelta
      }
      if visibleSouthDelta < 0 {
        deltaY -= visibleSouthDelta
      }
      map.setVisibleMapRect(MKMapRectOffset(visibleMapRect, deltaX, deltaY), animated: true)
    }
  }

// MARK: MKMapViewDelegate protocol

func mapView(mapView: MKMapView!, rendererForOverlay overlay: MKOverlay!) -> MKOverlayRenderer! {
  for layer in self.layers {
    if let renderer = layer.getRendererForOverlay(overlay) {
      return renderer
    }
  }
  return nil
}

func mapView(mapView: MKMapView!, regionWillChangeAnimated animated: Bool) {
  if self.mapView! === mapView {
    //self.saveMapState()
  }
}

func mapView(mapView: MKMapView!, regionDidChangeAnimated animated: Bool) {
  if self.mapView! === mapView {
    if !self.changingMap {
      self.checkScroll()
    }
    
  }
}

func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
  
  let centerCoordinate = annotation.coordinate
  let centerCGPoint = self.mapView!.convertCoordinate(centerCoordinate, toPointToView: self.mapView!)
  let touchRect = CGRectMake(centerCGPoint.x-4, centerCGPoint.y, 3, 3)
  let view = MKAnnotationView(frame: touchRect)
  view.backgroundColor = UIColor.clearColor()
  view.canShowCallout = false
  return view
}

func mapView(mapView: MKMapView!, didSelectAnnotationView view: MKAnnotationView!) {
  println("selected: \(view.annotation.title!)")
  for layer in self.layers {
    let id = view.annotation.title!.toInt()!
    if layer.shapes.has(id) {
     let touched = layer.shapes[id]
    touched!.fillinCallout(self.callout!)
    self.callout!.presentCalloutFromRect(view.bounds, inView: view, constrainedToView: self.view, animated: true)
    }
  }
}

func mapView(mapView: MKMapView!, didDeselectAnnotationView view: MKAnnotationView!) {
  println("deselected: \(view.annotation.title!)")
  self.callout!.dismissCalloutAnimated(true)
}

// MARK: SMCalloutViewDelegate protocol
func calloutView(calloutView: SMCalloutView!, delayForRepositionWithSize offset: CGSize) -> NSTimeInterval {
  let map = self.mapView!
  let currentCenterCoordinate = map.centerCoordinate
  let currentCenterCGpoint = map.convertCoordinate(currentCenterCoordinate, toPointToView: map)
  var newCenterCGpoint = currentCenterCGpoint
  newCenterCGpoint.x -= offset.width
  newCenterCGpoint.y -= offset.height
  let newCenterCoordinate = map.convertPoint(newCenterCGpoint, toCoordinateFromView: map)
  map.setCenterCoordinate(newCenterCoordinate, animated: true)
  
  return kSMCalloutViewRepositionDelayForUIScrollView
}


}

