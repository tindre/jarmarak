//
//  OSMMKMap.swift
//  krasnale
//
//  Created by Jakub Tomanik on 10/12/2014.
//  Copyright (c) 2014 Jakub Tomanik. All rights reserved.
//

import Foundation

class MKBackgroundOverlay: MKPolygon {
  override func canReplaceMapContent() -> Bool {
    return true
  }
}