//
//  ViewController.swift
//  krasnale
//
//  Created by Jakub Tomanik on 14/11/2014.
//  Copyright (c) 2014 Jakub Tomanik. All rights reserved.
//
import Foundation
import UIKit
import CoreLocation
import MapKit

class ViewController: UIViewController, MKMapViewDelegate, SMCalloutViewDelegate, UIGestureRecognizerDelegate {
  
  @IBOutlet weak var mapView: SMMapView!
  
  let minimumAltitude = 305.626131658142
  
  var lockedMap = true
  
  var mapBoundsRect:MKMapRect?
  var mapBoundsRegion:MKCoordinateRegion?
  var mapFocusPoint:CLLocationCoordinate2D  = CLLocationCoordinate2D(latitude: 0, longitude: 0)
  
  var currentViewRect:MKMapRect?
  var currentViewRegion:MKCoordinateRegion?
  
  var defaultViewRect:MKMapRect?
  var defaultViewRegion:MKCoordinateRegion?
  
  var currentMapCenter:CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 0, longitude: 0)
  var currentMapSpan:MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: 0.001, longitudeDelta: 0.001)
  var currentAltitude:CLLocationDistance = 0.0
  var currentHeading:CLLocationDirection = 0.0
  
  var mapViewBounds:MKMapRect?
  
  var callout:SMCalloutView?
  var annotationView: MKAnnotationView?
  
  var layers = [OSMLayer]()
  
  override func viewDidLoad() {
    println("viewDidLoad")
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    
    let delegate = UIApplication.sharedApplication().delegate as AppDelegate
    
    // Gesture recognisers
    let oneTap = UITapGestureRecognizer(target: self, action:Selector("handleTap:"))
    oneTap.cancelsTouchesInView = false
    oneTap.numberOfTapsRequired = 1
    
    let doubleTap = UITapGestureRecognizer(target: self, action:Selector("handleTap:"))
    doubleTap.cancelsTouchesInView = false
    doubleTap.numberOfTapsRequired = 2
    
    let moreTap = UITapGestureRecognizer(target: self, action:Selector("moreTap"))
    moreTap.numberOfTapsRequired = 1
    
    let iconTap = UITapGestureRecognizer(target: self, action:Selector("iconTap"))
    iconTap.numberOfTapsRequired = 1
    
    let panMap = UIPanGestureRecognizer(target: self, action:Selector("interactWithMap:"))
    panMap.delegate = self
    let rotateMap = UIRotationGestureRecognizer(target: self, action:Selector("interactWithMap:"))
    rotateMap.delegate = self
    let pinchMap = UIPinchGestureRecognizer(target: self, action:Selector("interactWithMap:"))
    pinchMap.delegate = self
    
    self.mapView.addGestureRecognizer(panMap)
    self.mapView.addGestureRecognizer(rotateMap)
    self.mapView.addGestureRecognizer(pinchMap)
    
    self.mapView.addGestureRecognizer(oneTap)
    self.mapView.addGestureRecognizer(oneTap)
    
    oneTap.requireGestureRecognizerToFail(doubleTap)
    
    // config mapView
    self.mapView.delegate = self
    
    delegate.configXML!.iterateWithRootXPath("/settings/app/mapView/mapType", usingBlock: {(found: RXMLElement!) in
      self.mapView.mapType = MKMapType(rawValue: UInt(found.textAsInt))!
    })
    
    let xmlCenter = delegate.configXML!.child("map").child("center")
    let xmlLat = xmlCenter.attributeAsDouble("lat")
    let xmlLon = xmlCenter.attributeAsDouble("lon")
    if xmlLat != 0 && xmlLon != 0 {
      self.mapFocusPoint.latitude = xmlLat
      self.mapFocusPoint.longitude = xmlLon
    }
    
    let xmlBounds = delegate.configXML!.child("map").child("bounds")
    if let tmpBounds = OSMBounds(element: xmlBounds) {
      let top = tmpBounds.maxLatitude
      let bottom = tmpBounds.minLatitude
      let left = tmpBounds.minLongitude
      let right = tmpBounds.maxLongitude
      
      let originCoordinate = CLLocationCoordinate2D(latitude: top, longitude: left)
      let maxCoordinate = CLLocationCoordinate2D(latitude: bottom, longitude: right)
      let originMapPoint = MKMapPointForCoordinate(originCoordinate)
      let maxMapPoint = MKMapPointForCoordinate(maxCoordinate)
      
      self.mapBoundsRect = MKMapRectMake(originMapPoint.x, originMapPoint.y, maxMapPoint.x - originMapPoint.x, maxMapPoint.y - originMapPoint.y)
      self.mapBoundsRegion = MKCoordinateRegionForMapRect(self.mapBoundsRect!)
      
      
      let initialRegion = MKCoordinateRegionMake(self.mapFocusPoint, self.currentMapSpan)
      self.mapView.region = initialRegion
      self.currentViewRect = self.mapView.visibleMapRect
      self.currentViewRegion = self.mapView.region
      self.defaultViewRect = self.mapView.visibleMapRect
      self.defaultViewRegion = self.mapView.region
    }
    
    // Load layers
    let xmlLayers = delegate.configXML!.child("map").child("layers").children("layer")
    for xmlLayer in xmlLayers {
      let layerSource = xmlLayer.attribute("src")
      let layerStyleSource = xmlLayer.attribute("style")
      let layerPath = delegate.payloadPath! + layerSource
      let layerStylePath = delegate.payloadPath! + layerStyleSource
      
      if let layer = OSMLayer(path: layerPath) {
        self.layers.append(layer)
        layer.loadStyles(layerStylePath)
        layer.buildShapes()
        layer.renderShapesInMap(self.mapView)
      }
    }
    
    // config callout
    self.callout = SMCalloutView.platformCalloutView()
    self.callout!.delegate = self
    self.callout!.title = "title"
    self.callout!.subtitle = "subtitle"
    let leftView = UIButton(frame: CGRectMake(0, 0, 44, 44+30))
    leftView.backgroundColor = UIColor(red: 0, green: 0.5, blue: 1, alpha: 1)
    if let icon = UIImage(named: "choinka1.png") {
      let iconView = UIImageView(image: icon)
      iconView.frame = CGRectMake(0, 0, 44, 44)
      iconView.tag = 44
      leftView.addSubview(iconView)
    }
    
    leftView.addGestureRecognizer(iconTap)
    self.callout?.leftAccessoryView = leftView
    
    self.mapView!.calloutView = self.callout!
    
  }
  
  override func  viewDidAppear(animated: Bool) {
    println("viewDidAppear")
    super.viewDidAppear(animated)
    let camera = MKMapCamera(lookingAtCenterCoordinate: self.mapFocusPoint, fromEyeCoordinate: self.mapFocusPoint, eyeAltitude: self.minimumAltitude)
    mapView.camera = camera
    self.lockedMap = false
    self.saveMapState()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  // MARK: private methodes
  func iconTap() {
    println("iconTap")
  }
  
  func moreTap() {
    println("moreTap")
  }
  
  func interactWithMap(gesture: UIGestureRecognizer) {
    switch gesture {
    case let action as UIPanGestureRecognizer:
      if action.state == UIGestureRecognizerState.Began {
        println("begin tap")
        self.saveMapState()
      }
    case let action as UIRotationGestureRecognizer:
      if action.state == UIGestureRecognizerState.Began {
        println("begin rotation")
        self.saveMapState()
      }
    case let action as UIPinchGestureRecognizer:
      if action.state == UIGestureRecognizerState.Began {
        println("begin pinch")
        self.saveMapState()
      }
    default:
      break
    }
    
  }
  
  func saveMapState(){
    println("saving...")
    self.currentViewRegion! = self.mapView!.region
    self.currentViewRect! = self.mapView!.visibleMapRect
    self.currentAltitude = self.mapView!.camera.altitude
    self.currentHeading = self.mapView!.camera.heading
    println("latitude: \(self.currentViewRegion!.center.latitude) longitude:\(self.currentViewRegion!.center.longitude)")
    println("latDelta \(self.currentViewRegion!.span.latitudeDelta) \(self.currentViewRegion!.span.longitudeDelta)")
    println("altitude \(self.currentAltitude), heading \(self.currentHeading)")
  }
  
  func handleTap(recognizer: UITapGestureRecognizer) {
    self.saveMapState()
    if recognizer.state == .Ended {
      
      // CG Units
      let tapPoint = recognizer.locationInView(self.mapView)
      let tapCoordinate = self.mapView.convertPoint(tapPoint, toCoordinateFromView: self.mapView)
      let tapMapPoint = MKMapPointForCoordinate(tapCoordinate)
      
      for layer in self.layers {
        if let touched = layer.checkForTouch(tapMapPoint) {
          println("touched: \(touched.element!.id)")
          touched.fillinCallout(self.callout!)
          self.mapView!.selectAnnotation(touched.shape as MKAnnotation, animated: true)
        } else {
          //self.callout!.dismissCalloutAnimated(true)
        }
      }
    }
  }
  
  // MARK: MKMapViewDelegate protocol
  
  func mapView(mapView: MKMapView!, rendererForOverlay overlay: MKOverlay!) -> MKOverlayRenderer! {
    println("rendererForOverlay")
    for layer in self.layers {
      if let renderer = layer.getRendererForOverlay(overlay) {
        return renderer
      }
    }
    return nil
  }
  
  func mapView(mapView: MKMapView!, regionWillChangeAnimated animated: Bool) {
    println("regionWillChangeAnimated")
  }
  
  func mapView(mapView: MKMapView!, regionDidChangeAnimated animated: Bool) {
    println("rregionDidChangeAnimated")
    if self.lockedMap == false {
        println("check map")
      
        let visibleMapRect = mapView.visibleMapRect
        let visibleRegion = mapView.region
        println("visible \(visibleMapRect.size.width) \(visibleMapRect.size.height)")
        
        // nothing to do
        if MKMapRectContainsRect(self.mapBoundsRect!, visibleMapRect) {
          println("within bounds")
          return
        }
        
        //check if zoomed out too much
        if visibleMapRect.size.width > self.mapBoundsRect!.size.width ||
          visibleMapRect.size.height > self.mapBoundsRect!.size.height {
            self.lockedMap = true
            println("scale map \(self.currentViewRect!.size.width) \(self.currentViewRect!.size.height)")
            let camera = MKMapCamera(lookingAtCenterCoordinate: self.currentViewRegion!.center, fromEyeCoordinate: self.currentViewRegion!.center, eyeAltitude: self.currentAltitude)
            camera.heading = self.currentHeading
            mapView.setCamera(camera, animated: true)
            println("new scale map \(mapView.visibleMapRect.size.width) \(mapView.visibleMapRect.size.height)")
            self.lockedMap = false
        } else {
          //self.lockedMap = true
          println("move map")
          //map.setVisibleMapRect(MKMapRectOffset(visibleMapRect, deltaX, deltaY), animated: true)
          println("move map center, lon: \(self.currentViewRegion!.center.longitude) lat: \(self.currentViewRegion!.center.latitude)")
          println("move map span, lon:\(self.currentViewRegion!.span.longitudeDelta) lat: \(self.currentViewRegion!.span.latitudeDelta)")
          //let test = MKCoordinateRegion(center: CLLocationCoordinate2DMake(51.1095944953005, 17.0308437033432), span: MKCoordinateSpan(latitudeDelta: 0.00155871464066593, longitudeDelta: 0.00377192220520328))
          //let test = map.regionThatFits(self.currentViewRegion!)
          //map.setRegion(test, animated: false)
          mapView.setCenterCoordinate(self.currentViewRegion!.center, animated: true)
          //println("test map center, lon: \(test.center.longitude) lat: \(test.center.latitude)")
          //println("test map span, lon:\(test.span.longitudeDelta) lat: \(test.span.latitudeDelta)")
          println("new map \(mapView.region.span.longitudeDelta) \(mapView.region.span.latitudeDelta)")
          //self.lockedMap = false
        }
      }
  }
  
  func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
    println("viewForAnnotation")
    let centerCoordinate = annotation.coordinate
    let centerCGPoint = self.mapView!.convertCoordinate(centerCoordinate, toPointToView: self.mapView!)
    let touchRect = CGRectMake(centerCGPoint.x-4, centerCGPoint.y, 3, 3)
    let view = MKAnnotationView(frame: touchRect)
    view.backgroundColor = UIColor.clearColor()
    view.canShowCallout = false
    return view
  }
  
  func mapView(mapView: MKMapView!, didSelectAnnotationView view: MKAnnotationView!) {
    println("didSelectAnnotationView")
    println("selected: \(view.annotation.title!)")
    for layer in self.layers {
      let id = view.annotation.title!.toInt()!
      if layer.shapes.has(id) {
        let touched = layer.shapes[id]
        touched!.fillinCallout(self.callout!)
        self.callout!.presentCalloutFromRect(view.bounds, inView: view, constrainedToView: self.view, animated: true)
      }
    }
  }
  
  func mapView(mapView: MKMapView!, didDeselectAnnotationView view: MKAnnotationView!) {
    println("didDeselectAnnotationView")
    println("deselected: \(view.annotation.title!)")
    self.callout!.dismissCalloutAnimated(true)
  }
  
  // MARK: SMCalloutViewDelegate protocol
  func calloutView(calloutView: SMCalloutView!, delayForRepositionWithSize offset: CGSize) -> NSTimeInterval {
    println("calloutView")
    let map = self.mapView!
    let currentCenterCoordinate = map.centerCoordinate
    let currentCenterCGpoint = map.convertCoordinate(currentCenterCoordinate, toPointToView: map)
    var newCenterCGpoint = currentCenterCGpoint
    newCenterCGpoint.x -= offset.width
    newCenterCGpoint.y -= offset.height
    let newCenterCoordinate = map.convertPoint(newCenterCGpoint, toCoordinateFromView: map)
    map.setCenterCoordinate(newCenterCoordinate, animated: true)
    
    return kSMCalloutViewRepositionDelayForUIScrollView
  }
  
  func gestureRecognizer(gestureRecognizer: UIGestureRecognizer,
    shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool{
      return true
  }
  
  
}

