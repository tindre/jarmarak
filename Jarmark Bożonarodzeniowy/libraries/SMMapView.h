#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "SMCalloutView.h"

//
// This controller demonstrates how to use SMCalloutView with MKMapView. It is a bit more complex
// than using SMCalloutView with a simple UIScrollView. We need to subclass MKMapView in order
// to provide all our features such as allowing touches on our callout.
//

// We need a custom subclass of MKMapView in order to allow touches on UIControls in our custom callout view.
//
// Custom Map View
//
// We need to subclass MKMapView in order to present an SMCalloutView that contains interactive
// elements.
//

@interface MKMapView (UIGestureRecognizer)

// this tells the compiler that MKMapView actually implements this method
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch;

@end
//@class SMMapView;

@interface SMMapView : MKMapView

@property (nonatomic, strong) SMCalloutView *calloutView;

@end
